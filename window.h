#ifndef WINDOW_H
#define WINDOW_H

#include <QtWidgets>
#include <QMainWindow>
#include <QTimer>
#include <QLCDNumber>
#include "board.h"
#include "minesweeperbutton.h"

#define SIZE_OF_BUTTON 20
#define EASY_COLUMNS 10
#define EASY_ROWS 10
#define MODERATE_COLUMNS 15
#define MODERATE_ROWS 12
#define HARD_COLUMNS 20
#define HARD_ROWS 15
#define BOMBS_ON_EASY 10
#define BOMBS_ON_MODERATE 30
#define BOMBS_ON_HARD 70

class QHBoxLayout;
class QVBoxLayout;

namespace Ui {
    class myWindow;
}

class myWindow : public QMainWindow
{
    Q_OBJECT

public:
    myWindow(QWidget *parent = 0);
private slots:
    void updateTimer();
    void handleEasy(); //For easy button-press
    void handleModerate(); //For moderate button-press
    void handleHard(); //For hard button-press
    void handleButtonReleased();
    void handleRightButton();
    //Slots for menu actions
    void open();
    bool save();
    bool saveAs();
    void about();
    void wasModified();
private:
    Board* board; //A pointer to Board objects, holds actual board
    int level; //Current game level
    int currentTime;
    int trueFlagCounter, trueClickedCounter;
    bool isUntitled,gameEnded;
    bool hasStarted;
    QString curFile; //Current file's name
    QTimer *timer;
    QLCDNumber *lcdNumber1;
    QWidget* centralWidget; //Holds basic window layout
    QVBoxLayout* layout; //Window layout
    QHBoxLayout* levelSelect; //Holds buttons easy,moderate,hard
    QHBoxLayout* counters;
    QStackedWidget* boardSelect; //Has boards for all levels stacked
    QVector< QVector< QVector<MineSweeperButton*> > > buttons; //Has pointers to all buttons

    //The two basic menus
    QMenu *fileMenu;
    QMenu *helpMenu;

    //Menu actions and submenus
    QMenu *newMenu; //New is a submenu of File menu
    QAction *newEasy;
    QAction *newModerate;
    QAction *newHard;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *aboutAct;
    QAction *aboutQtAct;

    QWidget *createPushButtonGrid(int pLevel, int f_cols, int f_rows); //Function that creates a button grid of size cols*rows

    void newGame(int);
    void createActions();
    void createMenus();
    void createStatusBar();
    void createToolbarAndBoard();
    bool maybeSave();
    void loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);

    void lose();
    void revealAll() const;
    void win();
};

#endif

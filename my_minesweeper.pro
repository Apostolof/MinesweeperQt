#-------------------------------------------------
#
# Project created by QtCreator 2015-12-20T11:50:09
#
#-------------------------------------------------

QT += widgets
QT += designer
RC_FILE = minesico.rc

HEADERS       = window.h \
    minesweeperbutton.h \
    board.h
SOURCES       = window.cpp \
                main.cpp \
    minesweeperbutton.cpp \
    board.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/widgets/groupbox
INSTALLS += target

RESOURCES += \
    resource.qrc

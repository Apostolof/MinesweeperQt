#ifndef MINESWEEPERBUTTON_H
#define MINESWEEPERBUTTON_H

#include<QPushButton>

class MineSweeperButton : public QPushButton
{
    Q_OBJECT
public:
    MineSweeperButton(QWidget* = 0);
    MineSweeperButton(QString);
    MineSweeperButton(int,int,QWidget* = 0);
    int getRow() const;
    int getColumn() const;
    bool isFlaged() const;
    void setFlaged(bool);
    void setClicked(bool);
signals:
    void rightButtonClicked();
protected:
    void mousePressEvent(QMouseEvent*);
private:
    int row,column;
    bool flaged,clicked;
};

#endif // MINESWEEPERBUTTON_H
